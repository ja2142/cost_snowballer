#!/usr/bin/env python3

balances = [
        ('Garry', -2),
        ('Ron', -3.5),
        ('Mark', -1),
        ('Alice', 2.2),
        ('Bob', 3.8),
        ('Ann', 0.5)
]

def snowball_cost(balances):
    # idea:
    # divide into those who pay and those who are paid
    # pick a payee, and redirect payers to that person until that person gets paid more than it should be
    # difference is paid to next payee
    # at the end, if sum of balance is 0, everyone's balance should check out

    assert(sum(map(lambda x: x[1], balances)) == 0)

    payers = list(filter(lambda x: x[1] < 0, balances))
    payees = list(filter(lambda x: x[1] > 0, balances))
    for payee in payees:
        payee_name = payee[0]
        balance = payee[1]
        while balance > 0:
            payer = payers.pop()
            payer_name = payer[0]
            paid_amount = -payer[1] # payer balance is negative, paid amount isn't
            print(f'{payer_name} pays {payee_name} {paid_amount}')
            balance -= paid_amount
        if balance < 0:
            # payee was paid too much, and will pay to someone else
            payers.append((payee_name, balance))

    assert(len(payers) == 0)

if __name__ == '__main__':
    snowball_cost(balances)
